﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebProject.Models
{
    public class HomeVar
    {
        public Users user { get; set; }
        public List<Users> listUsers {get;set;}
        public List<Budgets> listBudgets { get; set; }
        public List<Action> listActions { get; set; }
        public List<Joins> listJoins { get; set; }
        public Budgets budget {get;set;}
        public Action actionn { get; set; }
        public bool isAdmin { get; set; }
    }
}